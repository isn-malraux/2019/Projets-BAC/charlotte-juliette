import datetime
import random

signe = None
a_trouve_son_signe_astrologique = False
choix = "0"

def Trouve_ton_signe_astrologique():
    """
    Fonction permettant à l'utilisateur de saisir sa date de naissance et que son signe astrologique soit donné ensuite
    """
    
    global a_trouve_son_signe_astrologique
    while True:
         try:
            jour = int(input("Quel est votre jour de naissance ? "))
            break
         except ValueError:
            print("Oups, votre nombre n'esiste pas dans nos astres", "\n")
            jour = None
            
    while True:
         try:
            mois = int(input("Quel est votre mois de naissance ? "))
            break
         except ValueError:
            print("Oups, votre nombre n'esiste pas dans nos astres", "\n")
            mois = None
    
    while True:
         try:
            annee = int(input("Quel est votre année de naissance ? "))
            break
         except ValueError:
            print("Oups, votre nombre n'esiste pas dans nos astres" , "\n")
            annee = None
    while True:
        try:
            date_naissance = datetime.date(annee, mois, jour)
            break
        except ValueError:
            print("Votre date de naissance n'existe pas, veuillez la resaisir", "\n")
            Trouve_ton_signe_astrologique()
            
    if datetime.date(annee, 1, 21) <= date_naissance <= datetime.date(annee, 2, 18):
        signe = "Verseau"
        
    elif datetime.date(annee, 2, 19) <= date_naissance <= datetime.date(annee, 3, 20):
        signe = "Poisson"
        
    elif datetime.date(annee, 3, 21) <= date_naissance <= datetime.date(annee, 4, 20):
        signe = "Bélier"
           
    elif datetime.date(annee, 4, 21) <= date_naissance <= datetime.date(annee, 5, 20):
        signe = "Taureau"
        
    elif datetime.date(annee, 5, 21) <= date_naissance <= datetime.date(annee, 6, 21):
        signe = "Gémeaux"
           
    elif datetime.date(annee, 6, 22) <= date_naissance <= datetime.date(annee, 7, 22):
        signe = "Cancer"
           
    elif datetime.date(annee, 7, 23) <= date_naissance <= datetime.date(annee, 8, 22):
        signe = "Lion"
          
    elif datetime.date(annee, 8, 23) <= date_naissance <= datetime.date(annee, 9, 22):
        signe = "Vierge"
          
    elif datetime.date(annee, 9, 23) <= date_naissance <= datetime.date(annee, 10, 22):
        signe = "Balance"
          
    elif datetime.date(annee, 10, 23) <= date_naissance <= datetime.date(annee, 11, 22):
        signe = "Scorpion"
          
    elif datetime.date(annee, 11, 23) <= date_naissance <= datetime.date(annee, 12, 22):
        signe = "Sagittaire"
          
    elif datetime.date(annee, 12, 23) <= date_naissance <= datetime.date(annee, 1, 20):
        signe = "Capricorne"
    return signe
  
    
def Regarde_ton_horoscope() :
    if signe is None:
            print("Veuillez définir votre signe astrologique")
            afficherMenu()
    else :
            
        tab1 = ["des mensonges," , "de votre maladresse," , "des coups d'un soir," , "de votre comportement," , "de votre partenaire," , "de votre manque d’affection,"]
        tab2 = ["d'être déçu." , "de faire fuir." , "de le regretter." , "d'attraper une MST." , "de devenir cocu." , "d’être trop insistant."]
        tab3 = ["sans égale," , "douteuse," , "aléatoire," , "contagieuse," , "nulle,"]
        tab4 = ["de faire une pause." , "votre objectif." , "votre café." , "votre famille." , "vos atouts."]
        tab5 = ["à vous hydrater," , "à faire du sport," , "de manière positive," , "à respirer," , "à dormir,"]
        tab6 = ["atteint(e) du cancer." , "avez pris quelques kilos." , "fatigué(e)." , "à bout de souffle." , "vivant."]
    
        print ("♥ AMOUR ♥")
        print("Méfiez-vous" , tab1[random.randint(0,5)] , "vous risqueriez" , tab2[random.randint(0,5)] , "\n")
        print ("▶ TRAVAIL ◀")
        print ("Votre efficacité est" , tab3[random.randint(0,4)] , "n'oubliez pas" , tab4[random.randint(0,4)] , "\n")
        print ("◊ SANTÉ ◊")
        print ("Pensez" , tab5[random.randint(0,4)] , "vous êtes" , tab6[random.randint(0,4)] , "\n")

def Nettoyer_ecran():
    print('\n' * 5)

def afficherMenu():
    print("MENU")
    print("1 : Quel est ton signe astrologique ?")
    if a_trouve_son_signe_astrologique = True
        print("2 : Regarde ton horoscope")
    
while choix!="q":
    afficherMenu()
    choix=input("Choisissez une action :")
    
    if choix == "1":
        signe = Trouve_ton_signe_astrologique()
        print(signe)
    elif choix == "2":
        if signe is not None and a_trouve_son_signe_astrologique :
            Regarde_ton_horoscope()
    elif choix.upper() == "Q":
       Nettoyer_ecran()
       
Nettoyer_ecran()
       
       