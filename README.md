# Cahier des charges projet BAC *ASTROSCOPE*

Nous voulons pouvoir fournir le signe astrologique d'une personne grâce à sa date de naissance. 
<br/>Et par la suite, lui faire connaître son horoscope.

## Objectifs
Nous avons décidé de créer un programme capable de donner directement à partir de la date de naissance renseignée par l’utilisateur, le signe astrologique correspondant. 
<br/>Pour l’horoscope, une phrase sera générée aléatoirement parmi celles préalablement définies pour ce signe.

## Habitudes de travail
<br/>• Tenir un journal de bord pour voir l’évolution et l’avancement du projet dans le temps.
<br/>• Se donner des tâches à faire en une durée limitée pour garantir une avancée du projet et éviter un retard dans l’élaboration du programme.
<br/>• Toutes les recherches, avancements et dialogues entre Juliette et moi (Charlotte) devront être accessibles par tous sur le site de Framagit.

## Liste exhaustive des éléments et contraintes
<br/>• L’écriture du programme se fait sur Spyder3 (Python3).
<br/>• Le programme doit avoir une interface graphique simple et compréhensible avec un menu contenant les fonctionnalités.
<br/>• ASTROSCOPE ne doit pas contenir de bugs.
<br/>• Le programme doit être libre et adopter la licence GNU General Public License v3.0, au sens où il doit offrir les libertés suivantes : copie, analyse, modification et rediffusion des modification. Le programme ne peut pas devenir non-libre.

## Fonctionnalités
Le menu doit posséder les fonctionnalités suivantes :
<br/>→ Calculer votre signe astrologique : c'est la fonctionnalité qui permet de connaître le signe astrologique à partir du jour et du mois de naissance.
<br/>→ Quel est votre horoscope du jour : fonctionnalité dans laquelle on sélectionne un signe astrologique et qui nous donne son horoscope en générant une phrase aléatoire parmi celles préalablement définies pour chaque signe du zodiaque.
<br/>→ Quitter ASTROSCOPE : quitter le programme après avoir redemander confirmation à l'utilisateur.

<br/>À la fin de l'exécution des fonctionnalités "Calculer votre signe astrologique" et "Quel est votre horoscope du jour ?", inclure une fonctionnalité "Revenir au menu".
<br/>Cela permettra à l'utilisateur de revenir au menu et de choisir ou renouveler une fonctionnalité.

# Participantes
 - Juliette DUROT
 - Charlotte OCCHIO