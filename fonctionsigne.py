import datetime

def trouver_ton_signe_astrologique():
    jour = int(input("Quel est votre jour de naissance ? "))
    mois = int(input("Quel est votre mois de naissance ? "))
    annee = int(input("Quel est votre année de naissance ? "))
    date_naissance = datetime.date(annee, mois, jour)
    if datetime.date(annee, 1, 21) <= date_naissance <= datetime.date(annee, 2, 18):
        signe = "Verseau"
        print(signe)
    elif datetime.date(annee, 2, 19) <= date_naissance <= datetime.date(annee, 3, 20):
        signe = "Poisson"
        print(signe)
    elif datetime.date(annee, 3, 21) <= date_naissance <= datetime.date(annee, 4, 20):
        signe = "Bélier"
        print(signe)    
    elif datetime.date(annee, 4, 21) <= date_naissance <= datetime.date(annee, 5, 20):
        signe = "Taureau"
        print(signe)    
    elif datetime.date(annee, 5, 21) <= date_naissance <= datetime.date(annee, 6, 21):
        signe = "Gémeaux"
        print(signe)   
    elif datetime.date(annee, 6, 22) <= date_naissance <= datetime.date(annee, 7, 22):
        signe = "Cancer"
        print(signe)   
    elif datetime.date(annee, 7, 23) <= date_naissance <= datetime.date(annee, 8, 22):
        signe = "Lion"
        print(signe)  
    elif datetime.date(annee, 8, 23) <= date_naissance <= datetime.date(annee, 9, 22):
        signe = "Vierge"
        print(signe)  
    elif datetime.date(annee, 9, 23) <= date_naissance <= datetime.date(annee, 10, 22):
        signe = "Balance"
        print(signe)  
    elif datetime.date(annee, 10, 23) <= date_naissance <= datetime.date(annee, 11, 22):
        signe = "Scorpion"
        print(signe)  
    elif datetime.date(annee, 11, 23) <= date_naissance <= datetime.date(annee, 12, 22):
        signe = "Sagittaire"
        print(signe)  
    elif datetime.date(annee, 12, 23) <= date_naissance <= datetime.date(annee, 1, 20):
        signe = "Sagittaire"
        print(signe)  
  
       
trouver_ton_signe_astrologique()