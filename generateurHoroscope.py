"""Générateur d'horoscope aléatoire sur une base de données d'expressions"""
import random

def regarde_ton_horoscope() :
    
    tab1 = ["des mensonges," , "de votre maladresse," , "des coups d'un soir," , "de votre comportement," , "de votre partenaire," , "de votre manque d’affection,"]
    tab2 = ["d'être déçu." , "de faire fuir." , "de le regretter." , "d'attraper une MST." , "de devenir cocu." , "d’être trop insistant."]
    tab3 = ["sans égale," , "douteuse," , "aléatoire," , "contagieuse," , "nulle,"]
    tab4 = ["de faire une pause." , "votre objectif." , "votre café." , "votre famille." , "vos atouts."]
    tab5 = ["à vous hydrater," , "à faire du sport," , "de manière positive," , "à respirer," , "à dormir,"]
    tab6 = ["atteint(e) du cancer." , "avez pris quelques kilos." , "fatigué(e)." , "à bout de souffle." , "vivant."]
    
    print ("♥ AMOUR ♥")
    print("Méfiez-vous" , tab1[random.randint(0,5)] , "vous risqueriez" , tab2[random.randint(0,5)] , "\n")
    print ("▶ TRAVAIL ◀")
    print ("Votre efficacité est" , tab3[random.randint(0,4)] , "n'oubliez pas" , tab4[random.randint(0,4)] , "\n")
    print ("◊ SANTÉ ◊")
    print ("Pensez" , tab5[random.randint(0,4)] , "vous êtes" , tab6[random.randint(0,4)])

regarde_ton_horoscope()